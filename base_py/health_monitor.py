import time
from collections import OrderedDict

import rospy


class TimeInfo(object):
    def __init__(self):
        self.start = None
        self.elapsed = 0
        self.min_ms = 2**64 - 1
        self.max_ms = -self.min_ms
        self.calls = 0
        self.target_hz = None


class HealthMonitor(object):
    def __init__(self, node_name, base_node):
        self.node_name = node_name
        self.base_node = base_node
        self.times = OrderedDict()

    def tic(self, id_):
        if id_ not in self.times:
            ti = TimeInfo()
            ti.target_hz = self.base_node.get_private_param(id_ + '_target', -1)
            self.times[id_] = ti
        self.times[id_].start = time.time()

    def toc(self, id_):
        if id_ in self.times:
            if self.times[id_].start is None:
                raise ValueError('tic called without toc for %s', id_)
            delta_s = time.time() - self.times[id_].start
            self.times[id_].start = None
            delta_ms = delta_s * 1000
            self.times[id_].elapsed += delta_ms
            self.times[id_].calls += 1
            self.times[id_].min_ms = min(delta_ms, self.times[id_].min_ms)
            self.times[id_].max_ms = max(delta_ms, self.times[id_].max_ms)
            hz = 1/delta_s
            is_hitting_target_hz = (hz >= self.times[id_].target_hz)
            if not is_hitting_target_hz:
                rospy.logerr('execute function is not running fast enough;'
                             'Actual Hz: %.2f, Target Hz: %.2f',
                             hz, self.times[id_].target_hz)
            return delta_ms

        # TODO arguably an error
        return -1

    def get_target_hz(self, id_):
        return self.times[id_].target_hz

    def print_time_statistics(self):
        rospy.loginfo('Time Statistics for %s', self.node_name)
        rospy.loginfo('Name\t\tAvg Hz\t\tTarget Hz\t\tAvg ms\t\tMin ms\t\tMax ms\t\tCalls')
        for id_, ti in self.times.iteritems():
            if ti.calls == 0:
                rospy.loginfo('%s : toc(%s) was never called', id_, id_)
            else:
                average_elapsed = ti.elapsed/ti.calls
                average_hz = 1000./average_elapsed
                rospy.loginfo('%s\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%d',
                              id_,
                              average_hz,
                              ti.target_hz,
                              average_elapsed,
                              ti.min_ms,
                              ti.max_ms,
                              ti.calls)
