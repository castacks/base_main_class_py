import rospy

from .health_monitor import HealthMonitor


class BaseNode(object):
    def __init__(self, node_name, anonymous=False):
        self.node_name = node_name
        self.anonymous = anonymous

        self._failed = False
        self._execute_timer = None

        self.monitor = HealthMonitor(node_name, self)

        # TODO pass argc, argv explicitly?

    def _initialize(self):
        rospy.init_node(self.node_name,
                        anonymous=self.anonymous)
        rospy.on_shutdown(self._shutdown)

        # get the rate that execute should be called at
        execute_timer_rate = self.get_private_param('execute_target', None)
        if execute_timer_rate is None:
            rospy.logfatal('execute_target parameter was not set.')
            return False
        if execute_timer_rate <= 0.:
            rospy.logfatal("The execute_target parameter must be "
                           "larger than 0.0.")
            return False
        status = self.initialize()
        if not status:
            rospy.logfatal("The initialize() function failed.")
            return False
        hz = rospy.Duration(1./execute_timer_rate)
        self._execute_timer = rospy.Timer(hz, self._execute_timer_callback)
        return True

    def _execute_timer_callback(self, timer_event):
        status = self._execute()
        if not status:
            self._execute_timer.stop()

    def _execute(self):
        self.monitor.tic('execute')
        status = self.execute()
        self.monitor.toc('execute')
        return status and not self._failed

    def _shutdown(self):
        self.monitor.print_time_statistics()
        self.shutdown()

    def initialize(self):
        """ This function should be implemented by the user.
        It should return True unless an error occurred.
        """
        raise NotImplementedError()

    def execute(self):
        """ This function should be implemented by the user.
        It should return True unless an error occurred.
        """
        raise NotImplementedError()

    def shutdown(self):
        """ This function is optionally implemented by the user.
        """
        pass

    def fail(self, reason):
        """ Failing will stop execution and deinitialize. """
        rospy.logfatal('Node %s has failed. Reason: %s',
                       self.node_name, reason)
        self._failed = True

    def get_param(self, param_name, default_val=None):
        # TODO what if param_name is not there? Let it throw?
        return rospy.get_param(param_name, default_val)

    def get_private_param(self, param_name, default_val=None):
        return self.get_param('~'+param_name, default_val)

    def tic(self, id_):
        self.monitor.tic(id_)

    def toc(self, id_):
        self.monitor.toc(id_)

    def run(self):
        status = self._initialize()
        if not status:
            rospy.logfatal("The _initialize() function failed. Exiting.")
        rospy.spin()
