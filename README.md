# BaseNode (Python version)

This package contains the BaseNode for Python.
Python nodes should inherit from BaseNode and implement the following interface:

`initialize()`: This is where you should set up ROS related objects, like
publishers and subscribers, and check for ROS parameters.

`execute()`: This is where your main program logic should go. It gets called in
a loop at a rate set by the `execute_target` parameter in your node�s
namespace.

`shutdown()`: Do cleanup. This is optional.

Finally, you should not implement a `main()` function yourself. Instead,
the Python code should have this structure:

```python
#!/usr/bin/env python

import rospy
from base_py import BaseNode

class MyNode(BaseNode):
    def __init__(self):
        super(MyNode, self).__init__('mynode')

    def initialize(self):
        rospy.loginfo('initializing a bunch of stuff')
        return True

    def execute(self):
        rospy.loginfo('working hard')
        return True

    def shutdown(self):
        rospy.loginfo("I'm done here")


if __name__ == "__main__":
    node = MyNode()
    node.run()
```

Note that the node file should have its executable bit set.

For larger nodes, it is good to practice to implement the Node in a separate module,
i.e. somewhere in `mypkg/mynode.py`, and then simply have something like:


```python

#!/usr/bin/env python
from mypkg import MyNode
node = MyNode()
node.run()
```

See [example_node_py](https://bitbucket.org/castacks/example_node_py/src/master/) for an example.

This package also contains a `HealthMonitor` class. This lets you time code's
running time by calling `self.tic("name")` and `self.toc("name")`. When `tic("name")` is
called for the first time, a ROS parameter with the name `name_target` is
looked up, which indicates what the target update rate for the code between tic
and toc is. For now, a message is printed if the target rate is not achieved.


Author: Daniel Maturana dimatura@cmu.edu slack: dimatura

Based on [`BaseNode`](https://bitbucket.org/castacks/base_main_class/src/master/) by John Keller.
